const express = require("express");
const app = express();
const mercadopago = require("mercadopago");

//REPLACE WITH YOUR ACCESS TOKEN AVAILABLE IN: https://developers.mercadopago.com/panel/credentials
mercadopago.configurations.setAccessToken(
  "TEST-6873783779862228-120421-643a50f10d25f2c1ddb6e1314f40f4eb-212998180"
);

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static("../client"));

app.get("/", (req, res) => {
  res.status(200).sendFile("index.html");
});

app.post("/process_payment", (req, res) => {
  let payment_data = {
    transaction_amount: Number(req.body.transactionAmount),
    token: req.body.token,
    description: req.body.description,
    installments: Number(req.body.installments),
    payment_method_id: req.body.paymentMethodId,
    issuer_id: req.body.issuer,
    payer: {
      email: req.body.email,
      identification: {
        type: req.body.docType,
        number: req.body.docNumber,
      },
    },
  };

  //POSSIBLE PAYMENT STATUSSES: https://www.mercadopago.com.ar/developers/bundles/images/api-payment-status-es.png?v=dd499255a2b0c0631e3777ead1e01f78
  mercadopago.payment
    .save(payment_data)
    .then((response) => {
      res.status(response.status).json({
        status: response.body.status,
        status_detail: response.body.status_detail,
        id: response.body.id,
      });
    })
    .catch((error) => {
      res.status(response.status).send(error);
    });
});

app.listen(5000, () => {
  console.log("The server is now running on Port 5000");
});
