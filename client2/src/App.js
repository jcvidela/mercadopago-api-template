import React from "react";
// import axios from 'axios';

const App = () => {
  const handleSubmit = () => {
    console.log("hola desde submit");
    // axios.post('/process_payment');
  };
  return (
    <div>
      <form>
        <h3>Detalles del comprador</h3>
        <div>
          <div>
            <label htmlFor={"email"}>E-mail</label>
            <input
              id="email"
              name="email"
              type="text"
            />
          </div>
          <div>
            <label htmlFor={"docType"}>Tipo de documento</label>
            <select
              id="docType"
              name="docType"
              data-checkout="docType"
              type="text"
            />
          </div>
          <div>
            <label htmlFor={"docNumber"}>Número de documento</label>
            <input
              id="docNumber"
              name="docNumber"
              data-checkout="docNumber"
              type="text"
            />
          </div>
        </div>
        <h3>Detalles de la tarjeta</h3>
        <div>
          <div>
            <label htmlFor={"cardholderName"}>Titular de la tarjeta</label>
            <input
              id="cardholderName"
              data-checkout="cardholderName"
              type="text"
            />
          </div>
          <div>
            <label htmlFor={"true"}>Fecha de vencimiento</label>
            <div>
              <input
                type="text"
                placeholder="MM"
                id="cardExpirationMonth"
                data-checkout="cardExpirationMonth"
                onPaste={() => false}
                onCopy={() => false}
                onCut={() => false}
                onDrag={() => false}
                onDrop={() => false}
                autoComplete="off"
              />
              <span className="date-separator">/</span>
              <input
                type="text"
                placeholder="YY"
                id="cardExpirationYear"
                data-checkout="cardExpirationYear"
                onPaste={() => false}
                onCopy={() => false}
                onCut={() => false}
                onDrag={() => false}
                onDrop={() => false}
                autoComplete="off"
              />
            </div>
          </div>
          <div>
            <label htmlFor={"cardNumber"}>Número de la tarjeta</label>
            <input
              type="text"
              id="cardNumber"
              data-checkout="cardNumber"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
              autoComplete="off"
            />
          </div>
          <div>
            <label htmlFor={"securityCode"}>Código de seguridad</label>
            <input
              id="securityCode"
              data-checkout="securityCode"
              type="text"
              onPaste={() => false}
              onCopy={() => false}
              onCut={() => false}
              onDrag={() => false}
              onDrop={() => false}
              autoComplete="off"
            />
          </div>
          <div id="issuerInput">
            <label htmlFor="issuer">Banco emisor</label>
            <select id="issuer" name="issuer" data-checkout="issuer" />
          </div>
          <div>
            <label htmlFor="installments">Cuotas</label>
            <select type="text" id="installments" name="installments" />
          </div>
          <div>
            <input
              type="hidden"
              name="transactionAmount"
              id="transactionAmount"
              defaultValue={100}
            />
            <input type="hidden" name="paymentMethodId" id="paymentMethodId" />
            <input type="hidden" name="description" id="description" />
            <br />
            <input type="submit" onClick={handleSubmit} value="Pagar" />
            <br />
          </div>
        </div>
      </form>
    </div>
  );
};

export default App;
